import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { CodeativeNgxDocsComponent } from './codeative-ngx-docs/codeative-ngx-docs.component';

const routes: Route[] = [{
  path: '',
  redirectTo: 'docs',
  pathMatch: 'full'
}, {
  path: 'docs',
  component: CodeativeNgxDocsComponent
},
{
  path: 'modal',
  loadChildren: './codeative/modules/co-ngx-modal/co-ngx-modal.module#CoNgxModalModule'
}]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
