import { Injectable, ComponentRef, ComponentFactoryResolver, Injector, ApplicationRef, EmbeddedViewRef, Renderer2, RendererFactory2, Inject, TemplateRef, Type } from '@angular/core';
import { Component } from '@angular/compiler/src/core';
import { DOCUMENT } from '@angular/platform-browser';

export type Content<T> = string | TemplateRef<T> | Type<T>;

@Injectable({
  providedIn: 'root'
})
export class CoModalService {

  coModalWrapper = this.document.querySelector('co-ngx-modal');
  componentRef: any;
  private renderer: Renderer2

  constructor(
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    private appRef: ApplicationRef,
    @Inject(DOCUMENT) private document: Document,
    private rendererFactory: RendererFactory2
  ) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }


  show(component: any, inputs = {}, outputs = {}) {

    // Components configurations
    const configs: configs = { inputs, outputs };

    // Create a new component ref
    // const ngContent = this.resolveNgContent(content);
    this.componentRef = this.resolver.resolveComponentFactory(component).create(this.injector);

    console.log("Injected successfully!: ", this.componentRef)

    // Configure the component with the necessary inputs & outputs
    this.attachConfig(this.componentRef, configs);

    // Attach the component to the angular components tree
    this.appRef.attachView(this.componentRef.hostView);



    // Get a dom reference from the component

    // Get a dom reference from the component
    const { nativeElement } = this.componentRef.location;

    // Inject the component into co-ngx-modal
    // this.renderer.appendChild(this.coModalWrapper, componentDom);

    this.renderer.addClass(this.coModalWrapper, 'show');
    this.renderer.appendChild(this.document.querySelector('co-ngx-modal'), nativeElement)


    // Make the modal container visible


    return this.componentRef.instance;
  }

  hide() {
    this.componentRef.destroy();
    this.appRef.detachView(this.componentRef.hostView);
    // Make the modal container visible
    this.renderer.removeClass(this.coModalWrapper, 'show');
  }



  // resolveNgContent<T>(content: Content<T>) {
  //   if (typeof content === 'string') {
  //     const element = this.document.createTextNode(content);
  //     return [[element]];
  //   }

  //   if (content instanceof TemplateRef) {
  //     const viewRef = content.createEmbeddedView(null);
  //     return [viewRef.rootNodes];
  //   }
  // }

  // Attach all necessary inputs & outputs to the component
  attachConfig(ref: ComponentRef<Component>, configs: configs) {

    // Inputs
    for (const input in configs.inputs) {
      if (configs.inputs.hasOwnProperty(input)) {
        ref.instance[input] = configs.inputs[input];
      }
    }

    // Outputs
    for (const input in configs.outputs) {
      if (configs.outputs.hasOwnProperty(input)) {
        ref.instance[input] = configs.outputs[input];
      }
    }

  }



}


interface configs { inputs: Object; outputs: Object; }
