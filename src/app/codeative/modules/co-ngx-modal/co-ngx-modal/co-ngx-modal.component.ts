import { Component, OnInit, ViewContainerRef, ElementRef } from '@angular/core';
import { CoModalService } from '../co-modal.service';

@Component({
  selector: 'co-ngx-modal',
  templateUrl: './co-ngx-modal.component.html',
  styleUrls: ['./co-ngx-modal.component.scss']
})
export class CoNgxModalComponent implements OnInit {

  constructor(private modal: CoModalService, public el: ElementRef) { }

  ngOnInit() {
  }


  showModal () {
  }

  closeModal() {
    this.modal.hide();
  }

}
