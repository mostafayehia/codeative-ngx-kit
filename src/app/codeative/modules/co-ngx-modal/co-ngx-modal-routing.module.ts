import { RouterModule, Route } from '@angular/router';
import { NgModule } from '@angular/core';
import { CoModalDocComponent } from './co-modal-doc/co-modal-doc.component';


const routes: Route[] = [{
  path: '',
  component: CoModalDocComponent
}]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CoNgxModalRoutingModule { }
