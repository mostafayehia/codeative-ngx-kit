import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { CoNgxModalRoutingModule } from './co-ngx-modal-routing.module';

// Component
import { CoNgxModalComponent } from './co-ngx-modal/co-ngx-modal.component';
import { CoModalDocComponent } from './co-modal-doc/co-modal-doc.component';

@NgModule({
  imports: [
    CommonModule,
    CoNgxModalRoutingModule
  ],
  declarations: [CoModalDocComponent, CoNgxModalComponent],
  exports: [CoModalDocComponent, CoNgxModalComponent]
})
export class CoNgxModalModule { }
