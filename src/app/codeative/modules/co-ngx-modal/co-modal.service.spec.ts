/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CoModalService } from './co-modal.service';

describe('Service: CoModalInjector', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoModalService]
    });
  });

  it('should ...', inject([CoModalService], (service: CoModalService) => {
    expect(service).toBeTruthy();
  }));
});
