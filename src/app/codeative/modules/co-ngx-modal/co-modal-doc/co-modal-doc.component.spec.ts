/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CoModalDocComponent } from './co-modal-doc.component';

describe('CoModalDocComponent', () => {
  let component: CoModalDocComponent;
  let fixture: ComponentFixture<CoModalDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoModalDocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoModalDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
