import { Component, OnInit } from '@angular/core';
import { CoModalService } from '../co-modal.service';
import { DemoComponentComponent } from '../demo-component/demo-component.component';

@Component({
  selector: 'app-co-modal-doc',
  templateUrl: './co-modal-doc.component.html',
  styleUrls: ['./co-modal-doc.component.scss']
})
export class CoModalDocComponent implements OnInit {

  constructor(private coModal: CoModalService) { }

  ngOnInit() {
  }


  showModal () {
    this.coModal.show(DemoComponentComponent)
  }

}
