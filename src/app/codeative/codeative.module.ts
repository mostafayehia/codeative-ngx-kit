import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { CoNgxModalModule } from './modules/co-ngx-modal/co-ngx-modal.module';

import { DemoComponentComponent } from './modules/co-ngx-modal/demo-component/demo-component.component';

const MODULES = [CoNgxModalModule]

@NgModule({
  imports: [
    CommonModule,
    ...MODULES
  ],
  declarations: [DemoComponentComponent],
  exports: MODULES
})
export class CodeativeModule { }
