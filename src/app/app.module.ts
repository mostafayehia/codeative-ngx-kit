import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CodeativeNgxDocsComponent } from './codeative-ngx-docs/codeative-ngx-docs.component';
import { CodeativeModule } from './codeative';
import { DemoComponentComponent } from './codeative/modules/co-ngx-modal/demo-component/demo-component.component';

@NgModule({
   declarations: [
      AppComponent,
      CodeativeNgxDocsComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      CodeativeModule,
   ],
   providers: [],
   entryComponents: [DemoComponentComponent],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
